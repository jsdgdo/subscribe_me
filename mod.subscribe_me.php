<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Subscribe_me
	{
		public $return_data = '';

		public function __construct()
		{
			$this->EE =& get_instance();
		}
		/**
		* Set the form that will have the Name and Email
		* @return form_declaration
		*/
		function set_form(){

			$variables = array(
				'subscriber_name',
				'subscriber_email',
				'subscriber_country',
				'subscriber_language' 
			);

			$form_data = array(
				'id' => 'newsletter',
				'hidden_fields' => array(
					'ACT' => $this->EE->functions->fetch_action_id('Subscribe_me', 'new_subscriber')
				)
			);

			return $this->EE->functions->form_declaration($form_data) . $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $variables) . '</form>';
		}
		/**
		* Proccess a submitted subscriber and redirects to the referer 
		* @return Ajax response
		*/
		function new_subscriber()
    	{
    		//JS placeholders won't let empty values to be uploaded, these are the default values. 
    		$valores = ['Nombre y Apellido', 'Name', 'Nome', 'Email']; 
    		$hay_error = false;

    		if (in_array($_POST['name'], $valores) || in_array($_POST['email'], $valores))
    		{
    			$hay_error = true;
    		}

    		$data = array (
    			'subscriber_name' => $_POST['name'],
    			'subscriber_email' => $_POST['email'],
    			'subscriber_country' => $_POST['country'],
    			'subscriber_language' => $_POST['language'], 
    			'subscriber_date' => date("Y-m-d H:i:s")
    		);

    		if ($hay_error)
    		{
    			$message = 'Hubo un problema. Inténtelo más tarde';
    		}
    		else
    		{
    			if ($this->EE->db->insert('subscribe_me', $data)){
	    			$message = 'sent';
	    		}else{
	    			$message = 'Hubo un problema. Inténtelo más tarde';
	    		}
    		}

    		$this->EE->output->send_ajax_response($message);
    	}
	}
/* End of file pi.subscribe_me.php */  