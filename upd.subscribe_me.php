<?php
class Subscribe_me_upd
{
	var $version = '1.0';
	function __construct()
	{
		$this->EE =& get_instance();
	}
	function install()
	{
		$this->EE->load->dbforge();

		$data = array(
			'module_name' => 'Subscribe_me' , 
			'module_version' => $this->version,
			'has_cp_backend' => 'y'
		);
		$this->EE->db->insert('modules', $data);

		
		$data = array(
    		'class'     => 'Subscribe_me' ,
    		'method'    => 'new_subscriber'
		);
		$this->EE->db->insert('actions', $data);

	
		
		$fields = array(
			'subscriber_id' => array('type' => 'int','constraint' => '10', 'unsigned' => TRUE, 'auto_increment' => TRUE),
			'subscriber_name' => array('type' => 'varchar', 'constraint' => '200'), 
			'subscriber_email' => array('type' => 'varchar', 'constraint' => '200'),
			'subscriber_country' => array('type' => 'varchar', 'constraint' => '50'),
			'subscriber_language' => array('type' => 'varchar', 'constraint' => '100'),
			'subscriber_date' => array('type' => 'date')
		);
		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('subscriber_id', TRUE);
		$this->EE->dbforge->create_table('subscribe_me');

		$this->EE->load->library('layout');
		$this->EE->layout->add_layout_tabs($this->tabs(), 'Subscribe_me');		

		return TRUE;
	}
	function uninstall()
	{
		$this->EE->load->dbforge();
    	$this->EE->db->where('module_name', 'Subscribe_me');
		$this->EE->db->delete('modules');
		$this->EE->db->where('class', 'Subscribe_me');
		$this->EE->db->delete('actions');
		$this->EE->dbforge->drop_table('subscribe_me');
		$this->EE->load->library('layout');
		$this->EE->layout->delete_layout_tabs($this->tabs(), 'Subscribe_me');

		return TRUE;
	}

	function update($current = '')
	{
    	return FALSE;
	}

	function tabs(){
		$tabs['Subscribe_me'] = array(
        'index'=> array(
            'visible'   => 'true',
            'collapse'  => 'false',
            'htmlbuttons'   => 'true',
            'width'     => '100%'
            )
        );

        return $tabs;
	}
}
/* End of file upd.subscribe_me.php */