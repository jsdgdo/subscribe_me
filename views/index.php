<style type="text/css">
	div.wrapper table {
    	border-collapse: collapse; 
	}
	div.wrapper table tr.title {
		background: #27343C;
        color: #fff;
        border: none; 
   	}
    div.wrapper table tr 
    {
      border-bottom: 1px solid #27343C;
      color: #3b4f5b; 
  	}
    div.wrapper table tr td {
		padding: 5px; 
	}
	select{
		text-transform: capitalize;
	}
	input[name="field"], input[name="d_field"], input[name="d_filter"]{
		display: none;
	}
	div.wrapper p{
		float: left;
		margin-right: 5px;
	}
</style>
<div class="wrapper">
	<table>            
		<tr class="Title">
			<td>
				Nombre
			</td> 
			<td>
				E-mail
			</td>            
			<td>
				Fecha Alta
			</td>
			<td>Idioma</td>
			<td>Pais</td>
		</tr>		
		<?php 
			foreach ($results as $res){
		?>
			<tr>
				<td>
					<?php echo $res['subscriber_name'];?>
				</td>
				<td>
					<?php echo $res['subscriber_email'];?>
				</td>
				<td>
					<?php echo $res['subscriber_date'];?>
				</td>
				<td>
					<?php echo $res['subscriber_language'];?>
				</td>
				<td>
					<?php echo $res['subscriber_country'];?>
				</td>
			</tr>
		<?php
			}
		?>
	</table>
	<br />
	<?=form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=subscribe_me'.AMP.'method=return_filter_results')?>
		<p>Filtrar por: </p>
		<select name="filter">
			<?php 
				foreach ($filters as $res) {
					if (strpos($res, '_')){
						?>
							<option value="<?php echo $res ?>" class="title">
						<?php
								$temp = explode('_',$res);
                    			echo $temp[1];
                    	?>
                    		</option>
                    	<?php 
					}else{
						?>
							<option value="<?php echo $res ?>" class="value">
								&nbsp;&nbsp;&nbsp;<?php echo $res;?>
							</option>
						<?php
					}	
				}
			?>
		</select>
		<input type="text" name="field" value="" />
		<input type="submit" value="Visualizar" <?php echo $filter_enabled;?>/>
	<?=form_close()?>
	<br />
	<?=form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=subscribe_me'.AMP.'method=download_now')?>
		<input type="text" name="d_field" value="<?php if (isset($field)) { echo $field; } else { echo '';}?>">
		<input type="text" name="d_filter" value="<?php if (isset($filter)) { echo $filter; } else { echo '';}?>">
		<input type="submit" value="Descargar" <?php echo $download_enabled;?> />
	<?=form_close()?>
	<br />
	<?=form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=subscribe_me'.AMP.'method=download_all')?>
		<input type="submit" value="Descargar Todos" <?php echo $download_all_enabled;?>/>
	<?=form_close()?>
</div>                                          
<script type="text/javascript">
	$(document).ready(function(){
		if ($('select[name="filter"]').children('option:first').prop('selected') == true){
			$('select[name="filter"]').children('option.value:first').prop('selected', true);
			$('input[name="field"]').val($('select[name="filter"]').children('option:first').val());
		}
		$('select[name="filter"]').change(function(){
			$(this).children('option:selected').each(function(){
				if ($(this).hasClass('title')){
					$('input[name="field"]').val($(this).val());
					$(this).next('.value').prop('selected', true);
				}else if ($(this).hasClass('value')){
					$('input[name="field"]').val($(this).prevAll('.title:first').val());
				}
			});
			console.log($('input[name="field"]').val());
		});
	});
</script>