<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscribe_me_mcp
{
	function Subscribe_me_mcp()
    {
        $this->EE =& get_instance();
    }

    function index()
    {
        $this->EE->load->helper('form');
        $this->EE->cp->set_variable('cp_page_title', 'Subscribe me');
        $ret = $this->do_my_queries("SELECT * FROM exp_subscribe_me");
        if (count($ret) > 0){
            $fe = 'enabled';
            $dae = 'enabled';
        }else{ 
            $fe = 'disabled';
            $dae = 'disabled';
        }
        $de = 'disabled';
        $this->EE->load->library('table');
        return $this->EE->load->view("index",array('results'=>$ret, 'filters' => $this->return_filters(), 'download_enabled' => $de, 'filter_enabled' => $fe, 'download_all_enabled' => $dae), TRUE);
    }


    function return_filter_results()
    {
        if (isset($_POST['field']) && isset($_POST['filter'])){
            $q = "SELECT * FROM exp_subscribe_me where ".$_POST['field']." = '".$_POST['filter']."'";
        }else{
            $q = "SELECT * FROM exp_subscribe_me";
        }
        
        $key = $_POST['filter'];
        $this->EE->load->helper('form');
        $this->EE->cp->set_variable('cp_page_title', 'Subscribe me');
        $ret = $this->do_my_queries($q);
        if (count($ret) > 0){
            $fe = 'enabled';
            $de = 'enabled';
            $dae = 'enabled';
        }else{ 
            $fe = 'disabled';
            $de = 'disabled';
            $dae = 'disabled';
        }
        $this->EE->load->library('table');
        return $this->EE->load->view("index",array('results'=>$ret, 'filters' => $this->return_filters(), 'field' => $_POST['field'], 'filter' => $_POST['filter'], 'download_enabled' => $de, 'filter_enabled' => $fe, 'download_all_enabled' => $dae), TRUE);
    }


    function return_filters()
    {
        $query = $this->EE->db->query("select * from information_schema.columns where table_name = 'exp_subscribe_me'");
        $res = array();
        if ($query->num_rows() > 0){
            $counter = 0;
            foreach ($query->result_array() as $row) {
                if ($row['COLUMN_NAME'] != 'subscriber_name' && $row['COLUMN_NAME'] != 'subscriber_email' && $row['COLUMN_NAME'] != 'subscriber_id'){
                    $values_query = $this->EE->db->query("select ".$row['COLUMN_NAME']." from exp_subscribe_me group by ".$row['COLUMN_NAME']);
                    if ($values_query->num_rows() > 0){
                        $res[$counter] = $row['COLUMN_NAME'];
                        $counter++;
                        foreach ($values_query->result_array() as $values_row) {
                            $res[$counter] = $values_row[$row['COLUMN_NAME']];
                            $counter++;
                        }
                    }
                }
            }
        }
        return $res;
    }


    function do_my_queries($q)
    {
        $query = $this->EE->db->query($q);
        $ret = array();
        if ($query->num_rows() > 0){
            foreach($query->result_array() as $row){
                $ret[$row['subscriber_id']] = array('subscriber_name' => $row['subscriber_name'], 'subscriber_email' => $row['subscriber_email'], 'subscriber_country' => $row['subscriber_country'], 'subscriber_language' => $row['subscriber_language'], 'subscriber_date' => $row['subscriber_date']);
            }
        }
        return $ret;
    }


    function download_now()
    {
        $filename = 'subscribers.csv';
 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}");
        header("Expires: 0");
        header("Pragma: public");
         
        $fh = @fopen( 'php://output', 'w' );
         
        $headerDisplayed = false;

         if (isset($_POST['d_field']) && isset($_POST['d_filter'])){
             $q = "SELECT * FROM exp_subscribe_me where ".$_POST['d_field']." = '".$_POST['d_filter']."'";
         }else{
            $q = "SELECT * FROM exp_subscribe_me";
        }
         
        foreach ( $this->do_my_queries($q) as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }
         
            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;
    }

    function download_all(){
        $filename = 'all_subscribers.csv';
 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}");
        header("Expires: 0");
        header("Pragma: public");
         
        $fh = @fopen( 'php://output', 'w' );
         
        $headerDisplayed = false;

        $q = "SELECT * FROM exp_subscribe_me";
         
        foreach ( $this->do_my_queries($q) as $data ) {
            // Add a header row if it hasn't been added yet
            if ( !$headerDisplayed ) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }
         
            // Put the data into the stream
            fputcsv($fh, $data);
        }
        // Close the file
        fclose($fh);
        // Make sure nothing else is sent, our file is done
        exit;
    }
}
/*End of file mcp.subscribe_me.php */